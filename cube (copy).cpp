#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <glad/glad.h>
#include <FTGL/ftgl.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>

const int cubeVertex = 36;
GLuint textureID;

using namespace std;

  GLfloat u_xn = -120.0f;
  GLfloat u_xp = 120.0f;
  GLfloat u_yn = -60.0f;
  GLfloat u_yp = 60.0f;

struct VAO {
	GLuint VertexArrayID;
	GLuint VertexBuffer;
	GLuint ColorBuffer;
	GLuint TextureBuffer;
	GLuint TextureID;

	GLenum PrimitiveMode; // GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES, GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY and GL_TRIANGLES_ADJACENCY
	GLenum FillMode; // GL_FILL, GL_LINE
	int NumVertices;
};
typedef struct VAO VAO;

struct GLMatrices {
	glm::mat4 projection;
	glm::mat4 model;
	glm::mat4 view;
	GLuint MatrixID; // For use with normal shader
	GLuint TexMatrixID; // For use with texture shader
} Matrices;

struct FTGLFont {
	FTFont* font;
	GLuint fontMatrixID;
	GLuint fontColorID;
} GL3Font;

GLuint programID, fontProgramID, textureProgramID;

/* Function to load Shaders - Use it as it is */
GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path) {

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if(VertexShaderStream.is_open())
	{
		std::string Line = "";
		while(getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if(FragmentShaderStream.is_open()){
		std::string Line = "";
		while(getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	cout << "Compiling shader : " <<  vertex_file_path << endl;
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> VertexShaderErrorMessage( max(InfoLogLength, int(1)) );
	glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
	cout << VertexShaderErrorMessage.data() << endl;

	// Compile Fragment Shader
	cout << "Compiling shader : " << fragment_file_path << endl;
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> FragmentShaderErrorMessage( max(InfoLogLength, int(1)) );
	glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
	cout << FragmentShaderErrorMessage.data() << endl;

	// Link the program
	cout << "Linking program" << endl;
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> ProgramErrorMessage( max(InfoLogLength, int(1)) );
	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	cout << ProgramErrorMessage.data() << endl;

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}

static void error_callback(int error, const char* description)
{
	cout << "Error: " << description << endl;
}

void quit(GLFWwindow *window)
{
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}

glm::vec3 getRGBfromHue (int hue)
{
	float intp;
	float fracp = modff(hue/60.0, &intp);
	float x = 1.0 - abs((float)((int)intp%2)+fracp-1.0);

	if (hue < 60)
		return glm::vec3(1,x,0);
	else if (hue < 120)
		return glm::vec3(x,1,0);
	else if (hue < 180)
		return glm::vec3(0,1,x);
	else if (hue < 240)
		return glm::vec3(0,x,1);
	else if (hue < 300)
		return glm::vec3(x,0,1);
	else
		return glm::vec3(1,0,x);
}

/* Generate VAO, VBOs and return VAO handle */
struct VAO* create3DObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat* color_buffer_data, GLenum fill_mode=GL_FILL)
{
	struct VAO* vao = new struct VAO;
	vao->PrimitiveMode = primitive_mode;
	vao->NumVertices = numVertices;
	vao->FillMode = fill_mode;

	// Create Vertex Array Object
	// Should be done after CreateWindow and before any other GL calls
	glGenVertexArrays(1, &(vao->VertexArrayID)); // VAO
	glGenBuffers (1, &(vao->VertexBuffer)); // VBO - vertices
	glGenBuffers (1, &(vao->ColorBuffer));  // VBO - colors

	glBindVertexArray (vao->VertexArrayID); // Bind the VAO
	glBindBuffer (GL_ARRAY_BUFFER, vao->VertexBuffer); // Bind the VBO vertices
	glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW); // Copy the vertices into VBO
	glVertexAttribPointer(
						  0,                  // attribute 0. Vertices
						  3,                  // size (x,y,z)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	glBindBuffer (GL_ARRAY_BUFFER, vao->ColorBuffer); // Bind the VBO colors
	glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), color_buffer_data, GL_STATIC_DRAW);  // Copy the vertex colors
	glVertexAttribPointer(
						  1,                  // attribute 1. Color
						  3,                  // size (r,g,b)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	return vao;
}

/* Generate VAO, VBOs and return VAO handle - Common Color for all vertices */
struct VAO* create3DObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat red, const GLfloat green, const GLfloat blue, GLenum fill_mode=GL_FILL)
{
	GLfloat* color_buffer_data = new GLfloat [3*numVertices];
	for (int i=0; i<numVertices; i++) {
		color_buffer_data [3*i] = red;
		color_buffer_data [3*i + 1] = green;
		color_buffer_data [3*i + 2] = blue;
	}

	return create3DObject(primitive_mode, numVertices, vertex_buffer_data, color_buffer_data, fill_mode);
}

struct VAO* create3DTexturedObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat* texture_buffer_data, GLuint textureID, GLenum fill_mode=GL_FILL)
{
	struct VAO* vao = new struct VAO;
	vao->PrimitiveMode = primitive_mode;
	vao->NumVertices = numVertices;
	vao->FillMode = fill_mode;
	vao->TextureID = textureID;

	// Create Vertex Array Object
	// Should be done after CreateWindow and before any other GL calls
	glGenVertexArrays(1, &(vao->VertexArrayID)); // VAO
	glGenBuffers (1, &(vao->VertexBuffer)); // VBO - vertices
	glGenBuffers (1, &(vao->TextureBuffer));  // VBO - textures

	glBindVertexArray (vao->VertexArrayID); // Bind the VAO
	glBindBuffer (GL_ARRAY_BUFFER, vao->VertexBuffer); // Bind the VBO vertices
	glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW); // Copy the vertices into VBO
	glVertexAttribPointer(
						  0,                  // attribute 0. Vertices
						  3,                  // size (x,y,z)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	glBindBuffer (GL_ARRAY_BUFFER, vao->TextureBuffer); // Bind the VBO textures
	glBufferData (GL_ARRAY_BUFFER, 2*numVertices*sizeof(GLfloat), texture_buffer_data, GL_STATIC_DRAW);  // Copy the vertex colors
	glVertexAttribPointer(
						  2,                  // attribute 2. Textures
						  2,                  // size (s,t)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	return vao;
}

/* Render the VBOs handled by VAO */
void draw3DObject (struct VAO* vao)
{
	// Change the Fill Mode for this object
	glPolygonMode (GL_FRONT_AND_BACK, vao->FillMode);

	// Bind the VAO to use
	glBindVertexArray (vao->VertexArrayID);

	// Enable Vertex Attribute 0 - 3d Vertices
	glEnableVertexAttribArray(0);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->VertexBuffer);

	// Enable Vertex Attribute 1 - Color
	glEnableVertexAttribArray(1);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->ColorBuffer);

	// Draw the geometry !
	glDrawArrays(vao->PrimitiveMode, 0, vao->NumVertices); // Starting from vertex 0; 3 vertices total -> 1 triangle
}

void draw3DTexturedObject (struct VAO* vao)
{
	// Change the Fill Mode for this object
	glPolygonMode (GL_FRONT_AND_BACK, vao->FillMode);

	// Bind the VAO to use
	glBindVertexArray (vao->VertexArrayID);

	// Enable Vertex Attribute 0 - 3d Vertices
	glEnableVertexAttribArray(0);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->VertexBuffer);

	// Bind Textures using texture units
	glBindTexture(GL_TEXTURE_2D, vao->TextureID);

	// Enable Vertex Attribute 2 - Texture
	glEnableVertexAttribArray(2);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->TextureBuffer);

	// Draw the geometry !
	glDrawArrays(vao->PrimitiveMode, 0, vao->NumVertices); // Starting from vertex 0; 3 vertices total -> 1 triangle

	// Unbind Textures to be safe
	glBindTexture(GL_TEXTURE_2D, 0);
}
void updateProjection()
  {
      Matrices.projection = glm::ortho(u_xn, u_xp, u_yn, u_yp, 0.1f, 500.0f);
  }

  void zoomin()
  {
    u_xn += 3.0f;
    u_xp -= 3.0f;
    u_yn += 1.5f;
    u_yp -= 1.5f;
  }

  void zoomout()
  {
    u_xn -= 3.0f;
    u_xp += 3.0f;
    u_yn -= 1.5f;
    u_yp += 1.5f;
  }

/* Create an OpenGL Texture from an image */
GLuint createTexture (const char* filename)
{
	GLuint TextureID;
	// Generate Texture Buffer
	glGenTextures(1, &TextureID);
	// All upcoming GL_TEXTURE_2D operations now have effect on our texture buffer
	glBindTexture(GL_TEXTURE_2D, TextureID);
	// Set our texture parameters
	// Set texture wrapping to GL_REPEAT
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// Set texture filtering (interpolation)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// Load image and create OpenGL texture
	int twidth, theight;
	unsigned char* image = SOIL_load_image(filename, &twidth, &theight, 0, SOIL_LOAD_RGB);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D); // Generate MipMaps to use
	SOIL_free_image_data(image); // Free the data read from file after creating opengl texture
	glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess it up

	return TextureID;
}


/**************************
 * Customizable functions *
 **************************/

float triangle_rot_dir = 1;
float cube_rot_dir = -1;
bool triangle_rot_status = true;
bool cube_rot_status = true;

/* Executed when a regular key is pressed/released/held-down */
/* Prefered for Keyboard events */
void keyboard (GLFWwindow* window, int key, int scancode, int action, int mods)
{
	// Function is called first on GLFW_PRESS.

	if (action == GLFW_RELEASE) {
		switch (key) {

			case GLFW_KEY_X:
				// do something ..
				break;
			default:
				break;
		}
	}
	else if (action == GLFW_PRESS) {
		switch (key) {
			case GLFW_KEY_ESCAPE:
				quit(window);
				break;
			default:
				break;
		}
	}
	else if(action == GLFW_REPEAT)
      {
        switch(key)
        {
          case GLFW_KEY_UP:
            zoomin();
            updateProjection();
            break;
          case GLFW_KEY_DOWN:
            zoomout();
            updateProjection();
            break;

          // case GLFW_KEY_LEFT:
          // move_left();
          // updateProjection();
          // break;
          
          // case GLFW_KEY_RIGHT:
          // move_right();
          // updateProjection();
          // break;
        }
      
	}
}

/* Executed for character input (like in text boxes) */
void keyboardChar (GLFWwindow* window, unsigned int key)
{
	switch (key) {
		case 'Q':
		case 'q':
			quit(window);
			break;
		default:
			break;
	}
}

/* Executed when a mouse button is pressed/released */
void mouseButton (GLFWwindow* window, int button, int action, int mods)
{
	switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT:
			if (action == GLFW_RELEASE)
				triangle_rot_dir *= -1;
			break;
		case GLFW_MOUSE_BUTTON_RIGHT:
			if (action == GLFW_RELEASE) {
				cube_rot_dir *= -1;
			}
			break;
		default:
			break;
	}
}


/* Executed when window is resized to 'width' and 'height' */
/* Modify the bounds of the screen here in glm::ortho or Field of View in glm::Perspective */
void reshapeWindow (GLFWwindow* window, int width, int height)
{
	int fbwidth=width, fbheight=height;
	/* With Retina display on Mac OS X, GLFW's FramebufferSize
	 is different from WindowSize */
	glfwGetFramebufferSize(window, &fbwidth, &fbheight);

	GLfloat fov = 90.0f;

	// sets the viewport of openGL renderer
	glViewport (0, 0, (GLsizei) fbwidth, (GLsizei) fbheight);

	// set the projection matrix as perspective
	/* glMatrixMode (GL_PROJECTION);
	 glLoadIdentity ();
	 gluPerspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1, 500.0); */
	// Store the projection matrix in a variable for future use
	// Perspective projection for 3D views
	// Matrices.projection = glm::perspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1f, 500.0f);

	// Ortho projection for 2D views
	Matrices.projection = glm::ortho(-4.0f, 4.0f, -4.0f, 4.0f, 0.1f, 500.0f);
}





VAO *triangle, *rectangle;
// Creates the triangle object used in this sample code



void createTriangle ()
{
	/* ONLY vertices between the bounds specified in glm::ortho will be visible on screen */

	/* Define vertex array as used in glBegin (GL_TRIANGLES) */
	static const GLfloat vertex_buffer_data [] = {
		0, 1,0, // vertex 0
		-1,-1,0, // vertex 1
		1,-1,0, // vertex 2
	};

	static const GLfloat color_buffer_data [] = {
		1,0,0, // color 0
		0,1,0, // color 1
		0,0,1, // color 2
	};

	// create3DObject creates and returns a handle to a VAO that can be used later
	triangle = create3DObject(GL_TRIANGLES, 3, vertex_buffer_data, color_buffer_data, GL_LINE);
}

// Creates the rectangle object used in this sample code
void createRectangle (GLuint textureID)
{
	// GL3 accepts only Triangles. Quads are not supported
	static const GLfloat vertex_buffer_data [] = {
		-1.2,-1,0, // vertex 1
		1.2,-1,0, // vertex 2
		1.2, 1,0, // vertex 3

		1.2, 1,0, // vertex 3
		-1.2, 1,0, // vertex 4
		-1.2,-1,0  // vertex 1
	};

	static const GLfloat color_buffer_data [] = {
		1,0,0, // color 1
		0,0,1, // color 2
		0,1,0, // color 3

		0,1,0, // color 3
		0.3,0.3,0.3, // color 4
		1,0,0  // color 1
	};

	// Texture coordinates start with (0,0) at top left of the image to (1,1) at bot right
	static const GLfloat texture_buffer_data [] = {
		0,1, // TexCoord 1 - bot left
		1,1, // TexCoord 2 - bot right
		1,0, // TexCoord 3 - top right

		1,0, // TexCoord 3 - top right
		0,0, // TexCoord 4 - top left
		0,1  // TexCoord 1 - bot left
	};

	// create3DTexturedObject creates and returns a handle to a VAO that can be used later
	rectangle = create3DTexturedObject(GL_TRIANGLES, 6, vertex_buffer_data, texture_buffer_data, textureID, GL_FILL);
}

class Cube
  {
    private:
      float m_x, m_y, m_z, m_length, m_width, m_height;
      float m_minanglem, m_maxangle;
      VAO *m_cube;
      GLfloat m_vertex_buffer_data[3 * cubeVertex];
      GLfloat  m_color_buffer_data[3 * cubeVertex];
      float m_texture_buffer_data [2* cubeVertex];
    
    public:
      Cube(float x, float y, float z, float length, float width, float height);
      void createCube(GLfloat red, GLfloat blue, GLfloat green);
      void changeAngle(float angle);
      void draw();

  };

  Cube::Cube(float x, float y, float z,float length, float width, float height)
  {
    m_x = x;
    m_y = y;
    m_z = z;

    m_length = length;
    m_width = width;
    m_height = height;

  }

  void Cube::createCube(GLfloat red, GLfloat blue , GLfloat green )
  {
	m_vertex_buffer_data [0] = 0;
	m_vertex_buffer_data [1] = 0;
	m_vertex_buffer_data [2] = 0;

	m_vertex_buffer_data [3] = m_length;
	m_vertex_buffer_data [4] = 0;
	m_vertex_buffer_data [5] = 0;

	m_vertex_buffer_data [6] = m_length;
	m_vertex_buffer_data [7] = 0;
	m_vertex_buffer_data [8] = m_height;

	m_vertex_buffer_data [9] = m_length;
	m_vertex_buffer_data [10] = 0;
	m_vertex_buffer_data [11] = m_height;

	m_vertex_buffer_data [12] = 0;
	m_vertex_buffer_data [13] = 0;
	m_vertex_buffer_data [14] = m_height;

	m_vertex_buffer_data [15] = 0;
	m_vertex_buffer_data [16] = 0;
	m_vertex_buffer_data [17] = 0;
// bottom rectangle down!


	m_vertex_buffer_data [18] = 0; 
	m_vertex_buffer_data [19] = m_width;
	m_vertex_buffer_data [20] = 0;

	m_vertex_buffer_data [21] = m_length;
	m_vertex_buffer_data [22] = m_width;
	m_vertex_buffer_data [23] = 0;

	m_vertex_buffer_data [24] = m_length;
	m_vertex_buffer_data [25] = m_width;
	m_vertex_buffer_data [26] = m_height;

	m_vertex_buffer_data [27] = m_length;
	m_vertex_buffer_data [28] = m_width;
	m_vertex_buffer_data [29] = m_height;

	m_vertex_buffer_data [30] = 0;
	m_vertex_buffer_data [31] = m_width;
	m_vertex_buffer_data [32] = m_height;

	m_vertex_buffer_data [33] = 0;
	m_vertex_buffer_data [34] = m_width;
	m_vertex_buffer_data [35] = 0;
// Top rectangle done!

	m_vertex_buffer_data [36] = 0;
	m_vertex_buffer_data [37] = 0;
	m_vertex_buffer_data [38] = 0;

	m_vertex_buffer_data [39] = 0;
	m_vertex_buffer_data [40] = m_width;
	m_vertex_buffer_data [41] = 0;

	m_vertex_buffer_data [42] = 0;
	m_vertex_buffer_data [43] = m_width;
	m_vertex_buffer_data [44] = m_height;

	m_vertex_buffer_data [45] = 0;
	m_vertex_buffer_data [46] = m_width;
	m_vertex_buffer_data [47] = m_height;

	m_vertex_buffer_data [48] = 0;
	m_vertex_buffer_data [49] = 0;
	m_vertex_buffer_data [50] = m_height;

	m_vertex_buffer_data [51] = 0;
	m_vertex_buffer_data [52] = 0;
	m_vertex_buffer_data [53] = 0;
//left done


	m_vertex_buffer_data [54] = m_length;
	m_vertex_buffer_data [55] = 0;
	m_vertex_buffer_data [56] = 0;

	m_vertex_buffer_data [57] = m_length;
	m_vertex_buffer_data [58] = m_width;
	m_vertex_buffer_data [59] = 0;

	m_vertex_buffer_data [60] = m_length;
	m_vertex_buffer_data [61] = m_width;
	m_vertex_buffer_data [62] = m_height;

	m_vertex_buffer_data [63] = m_length;
	m_vertex_buffer_data [64] = m_width;
	m_vertex_buffer_data [65] = m_height;

	m_vertex_buffer_data [66] = m_length;
	m_vertex_buffer_data [67] = 0;
	m_vertex_buffer_data [68] = m_height;

	m_vertex_buffer_data [69] = m_length;
	m_vertex_buffer_data [70] = 0;
	m_vertex_buffer_data [71] = 0;
	//right done

	m_vertex_buffer_data [72] = 0;
	m_vertex_buffer_data [73] = m_width;
	m_vertex_buffer_data [74] = 0;

	m_vertex_buffer_data [75] = m_length;
	m_vertex_buffer_data [76] = m_width;
	m_vertex_buffer_data [77] = 0;

	m_vertex_buffer_data [78] = m_length;
	m_vertex_buffer_data [79] = 0;
	m_vertex_buffer_data [80] = 0;

	m_vertex_buffer_data [81] = m_length;
	m_vertex_buffer_data [82] = 0;
	m_vertex_buffer_data [83] = 0;

	m_vertex_buffer_data [84] = 0;
	m_vertex_buffer_data [85] = 0;
	m_vertex_buffer_data [86] = 0;

	m_vertex_buffer_data [87] = 0;
	m_vertex_buffer_data [88] = m_width;
	m_vertex_buffer_data [89] = 0;
//back done

	m_vertex_buffer_data [90] = 0;
	m_vertex_buffer_data [91] = m_width;
	m_vertex_buffer_data [92] = m_height;

	m_vertex_buffer_data [93] = m_length;
	m_vertex_buffer_data [94] = m_width;
	m_vertex_buffer_data [95] = m_height;

	m_vertex_buffer_data [96] = m_length;
	m_vertex_buffer_data [97] = 0;
	m_vertex_buffer_data [98] = m_height;

	m_vertex_buffer_data [99] = m_length;
	m_vertex_buffer_data [100] = 0;
	m_vertex_buffer_data [101] = m_height;

	m_vertex_buffer_data [102] = 0;
	m_vertex_buffer_data [103] = 0;
	m_vertex_buffer_data [104] = m_height;

	m_vertex_buffer_data [105] = 0;
	m_vertex_buffer_data [106] = m_width;
	m_vertex_buffer_data [107] = m_height;
	//front done


    for(int i = 0; i < cubeVertex; i++)
    {
      m_color_buffer_data[3*i + 0] = red;
      m_color_buffer_data[3*i + 1] = blue;
      m_color_buffer_data[3*i + 2] = green;
    }


		m_texture_buffer_data [0] = 0;
		m_texture_buffer_data [1] = 0;
		m_texture_buffer_data [2] = 1;
		m_texture_buffer_data [3] = 0;
		m_texture_buffer_data [4] = 1;
		m_texture_buffer_data [5] = 1;

		m_texture_buffer_data [6] = 1;
		m_texture_buffer_data [7] = 1;
		m_texture_buffer_data [8] = 0;
		m_texture_buffer_data [9] = 1;
		m_texture_buffer_data [10] = 0;
		m_texture_buffer_data [11] = 0;

		m_texture_buffer_data [12] = 0;
		m_texture_buffer_data [13] = 0;
		m_texture_buffer_data [14] = 1;
		m_texture_buffer_data [15] = 0;
		m_texture_buffer_data [16] = 1;
		m_texture_buffer_data [17] = 1;

		m_texture_buffer_data [18] = 1;
		m_texture_buffer_data [19] = 1;
		m_texture_buffer_data [20] = 0;
		m_texture_buffer_data [21] = 1;
		m_texture_buffer_data [22] = 0;
		m_texture_buffer_data [23] = 0;

		m_texture_buffer_data [24] = 0;
		m_texture_buffer_data [25] = 0;
		m_texture_buffer_data [26] = 1;
		m_texture_buffer_data [27] = 0;
		m_texture_buffer_data [28] = 1;
		m_texture_buffer_data [29] = 1;

		m_texture_buffer_data [30] = 1;
		m_texture_buffer_data [31] = 1;
		m_texture_buffer_data [32] = 0;
		m_texture_buffer_data [33] = 1;
		m_texture_buffer_data [34] = 0;
		m_texture_buffer_data [35] = 0;

		m_texture_buffer_data [36] = 0;
		m_texture_buffer_data [37] = 0;
		m_texture_buffer_data [38] = 1;
		m_texture_buffer_data [39] = 0;
		m_texture_buffer_data [40] = 1;
		m_texture_buffer_data [41] = 1;

		m_texture_buffer_data [42] = 1;
		m_texture_buffer_data [43] = 1;
		m_texture_buffer_data [44] = 0;
		m_texture_buffer_data [45] = 1;
		m_texture_buffer_data [46] = 0;
		m_texture_buffer_data [47] = 0;

		m_texture_buffer_data [48] = 0;
		m_texture_buffer_data [49] = 0;
		m_texture_buffer_data [50] = 1;
		m_texture_buffer_data [51] = 0;
		m_texture_buffer_data [52] = 1;
		m_texture_buffer_data [53] = 1;

		m_texture_buffer_data [54] = 1;
		m_texture_buffer_data [55] = 1;
		m_texture_buffer_data [56] = 0;
		m_texture_buffer_data [57] = 1;
		m_texture_buffer_data [58] = 0;
		m_texture_buffer_data [59] = 0;

		m_texture_buffer_data [60] = 0;
		m_texture_buffer_data [61] = 0;
		m_texture_buffer_data [62] = 1;
		m_texture_buffer_data [63] = 0;
		m_texture_buffer_data [64] = 1;
		m_texture_buffer_data [65] = 1;

		m_texture_buffer_data [66] = 1;
		m_texture_buffer_data [67] = 1;
		m_texture_buffer_data [68] = 0;
		m_texture_buffer_data [69] = 1;
		m_texture_buffer_data [70] = 0;
		m_texture_buffer_data [71] = 0;

    
    // m_cube = create3DObject(GL_TRIANGLES, cubeVertex, m_vertex_buffer_data, m_color_buffer_data, GL_FILL);

	// create3DTexturedObject creates and returns a handle to a VAO that can be used later
	m_cube = create3DTexturedObject(GL_TRIANGLES, cubeVertex, m_vertex_buffer_data, m_texture_buffer_data, textureID, GL_FILL);

  }

 
float camera_rotation_angle = 0;
float cube_rotation = 0;
float triangle_rotation = 0;


// you have to manage this fucntion to make it work
  void Cube::draw()
  {
  	// std::cout<<endl<<"cube draw";
    glm::mat4 VP = Matrices.projection * Matrices.view;

      glm::mat4 MVP;
      Matrices.model = glm::mat4(1.0f);
      
      // std::cout << rectangle_rotation <<endl;
      glm::mat4 translateCube = glm::translate (glm::vec3(-m_x-2, m_y , 0.0f));        // glTranslatef
      
      glm::mat4 rotateCube = glm::rotate((float)(cube_rotation*M_PI/180.0f), glm::vec3(0,0,1)); // rotate about vector (-1,1,1)
      Matrices.model *= (translateCube * rotateCube);
      MVP = VP * Matrices.model;
      glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);

	// Pop matrix to undo transformations till last push matrix instead of recomputing model matrix
	// glPopMatrix ();
	// Set the texture sampler to access Texture0 memory
	// glUniform1i(glGetUniformLocation(textureProgramID, "texSampler"), 0);

	// draw3DObject draws the VAO given to it using current MVP matrix
	draw3DTexturedObject(m_cube);

	// std::cout <<"Done";
      // draw3DObject draws the VAO given to it using current MVP matrix
      // draw3DObject(m_cube);

      // Increment angles
      float increments = 1;
      camera_rotation_angle++; // Simulating camera rotation

  }

 void drawBasics()
  {
  	// std::cout<<"Basics";
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    // glUseProgram (programID);
    glUseProgram(textureProgramID);

    // Eye - Location of camera. Don't change unless you are sure!!
    glm::vec3 eye ( 5*cos(camera_rotation_angle*M_PI/180.0f), 5, 5*sin(camera_rotation_angle*M_PI/180.0f) );
    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    // glm::vec3 eye(-10,-10,-10);
    glm::vec3 target (0, 0, 0);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    glm::vec3 up (0, 1, 0);

    // Compute Camera matrix (view)
    Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
    //  Don't change unless you are sure!!
    // Matrices.view = glm::lookAt(glm::vec3(0,0,3), glm::vec3(0,0,0), glm::vec3(0,1,0)); // Fixed camera for 2D (ortho) in XY plane
    // std::cout<<"basics complete";
  }
 


/* Initialise glfw window, I/O callbacks and the renderer to use */
/* Nothing to Edit here */
GLFWwindow* initGLFW (int width, int height)
{
	GLFWwindow* window; // window desciptor/handle

	glfwSetErrorCallback(error_callback);
	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(width, height, "Sample OpenGL 3.3 Application", NULL, NULL);

	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	glfwSwapInterval( 1 );

	/* --- register callbacks with GLFW --- */

	/* Register function to handle window resizes */
	/* With Retina display on Mac OS X GLFW's FramebufferSize
	 is different from WindowSize */
	glfwSetFramebufferSizeCallback(window, reshapeWindow);
	glfwSetWindowSizeCallback(window, reshapeWindow);

	/* Register function to handle window close */
	glfwSetWindowCloseCallback(window, quit);

	/* Register function to handle keyboard input */
	glfwSetKeyCallback(window, keyboard);      // general keyboard input
	glfwSetCharCallback(window, keyboardChar);  // simpler specific character handling

	/* Register function to handle mouse click */
	glfwSetMouseButtonCallback(window, mouseButton);  // mouse button clicks

	return window;
}

/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL (GLFWwindow* window, int width, int height)
{
	// Load Textures
	// Enable Texture0 as current texture memory
	glActiveTexture(GL_TEXTURE0);
	// load an image file directly as a new OpenGL texture
	// GLuint texID = SOIL_load_OGL_texture ("beach.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_TEXTURE_REPEATS); // Buggy for OpenGL3
	textureID = createTexture("cube_tex.jpg");
	// check for an error during the load process
	if(textureID == 0 )
		cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

	// Create and compile our GLSL program from the texture shaders
	textureProgramID = LoadShaders( "TextureRender.vert", "TextureRender.frag" );
	// Get a handle for our "MVP" uniform
	Matrices.TexMatrixID = glGetUniformLocation(textureProgramID, "MVP");


	/* Objects should be created before any other gl function and shaders */
	// Create the models
	createTriangle (); // Generate the VAO, VBOs, vertices data & copy into the array buffer
	createRectangle (textureID);


	// Create and compile our GLSL program from the shaders
	programID = LoadShaders( "Sample_GL3.vert", "Sample_GL3.frag" );
	// Get a handle for our "MVP" uniform
	Matrices.MatrixID = glGetUniformLocation(programID, "MVP");


	reshapeWindow (window, width, height);

	// Background color of the scene
	glClearColor (0.3f, 0.3f, 0.3f, 0.0f); // R, G, B, A
	glClearDepth (1.0f);

	glEnable (GL_DEPTH_TEST);
	glDepthFunc (GL_LESS);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Initialise FTGL stuff
	const char* fontfile = "arial.ttf";
	GL3Font.font = new FTExtrudeFont(fontfile); // 3D extrude style rendering

	if(GL3Font.font->Error())
	{
		cout << "Error: Could not load font `" << fontfile << "'" << endl;
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// Create and compile our GLSL program from the font shaders
	fontProgramID = LoadShaders( "fontrender.vert", "fontrender.frag" );
	GLint fontVertexCoordAttrib, fontVertexNormalAttrib, fontVertexOffsetUniform;
	fontVertexCoordAttrib = glGetAttribLocation(fontProgramID, "vertexPosition");
	fontVertexNormalAttrib = glGetAttribLocation(fontProgramID, "vertexNormal");
	fontVertexOffsetUniform = glGetUniformLocation(fontProgramID, "pen");
	GL3Font.fontMatrixID = glGetUniformLocation(fontProgramID, "MVP");
	GL3Font.fontColorID = glGetUniformLocation(fontProgramID, "fontColor");

	GL3Font.font->ShaderLocations(fontVertexCoordAttrib, fontVertexNormalAttrib, fontVertexOffsetUniform);
	GL3Font.font->FaceSize(1);
	GL3Font.font->Depth(0);
	GL3Font.font->Outset(0, 0);
	GL3Font.font->CharMap(ft_encoding_unicode);

	cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
	cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
	cout << "VERSION: " << glGetString(GL_VERSION) << endl;
	cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

}

int main (int argc, char** argv)
{
	int width = 1200;
	int height = 800;

	GLFWwindow* window = initGLFW(width, height);

	initGL (window, width, height);

	double last_update_time = glfwGetTime(), current_time;
	
	Cube c1(0,0,0,-1,1,1);
	c1.createCube(0.546f,0.355f,0.457f);

	/* Draw in loop */
	while (!glfwWindowShouldClose(window)) {
		
		// std::cout <<"loop"<<endl;

		drawBasics();
		// draw();
		// std::cout <<"draw basics"<<endl;
		c1.draw();
		// std::cout <<"draw circle"<<endl
		// Poll for Keyboard and mouse events
		glfwPollEvents();
		// Swap Frame Buffer in double buffering
		glfwSwapBuffers(window);

		// Control based on time (Time based transformation like 5 degrees rotation every 0.5s)
		
		}

	glfwTerminate();
	exit(EXIT_SUCCESS);
}
// 