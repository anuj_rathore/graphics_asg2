#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>	
#include <glm/gtc/matrix_transform.hpp>

#include <glad/glad.h>
#include <FTGL/ftgl.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <SFML/Audio.hpp>
float X =-5 ,Y =-5, Z=1;
const int cubeVertex = 36;

float bkp_Z = Z;
float fall_time;
GLuint textureID;
sf::Music buffer;

double xpos = 0;
double ypos = 0;

float hx=0,hy=0;
int coin = 1;
float motion = 1;
float tile_motion = 1;
int sign = 1;
int tile_sign =1;
time_t initial_time;

float s1=0;
float s2 =0;
float s3 = 0;
int jump = 0;
float speed = 0.1;

int camera = 1;
float x_eye = 0.25;
float y_eye = 0;
float z_eye = 5;

int c1=1,c2=1,c3=1;
int undo_keyboard = 0;

int spaces_x [5] = {-3,-2,0,2,3};
int spaces_y [5] = {-3,-2,-4,2,3};

int gold_x[3] = {-5,4,1};
int gold_y[3] = {-3,-2,-1};

int ghost_x[5] = {1,3,0,2,-2}; 
int ghost_y[5] = {-2,-3,0,1,2};

int tiles_x[] = {2,-2,2,-3,0,0}; 
int tiles_y[] = {3,0,0,3,-2,2};

int Movtile_x [2] = {-4,4};
int Movtile_y [2] = {1,-4};

using namespace std;

GLfloat u_xn = -120.0f;
GLfloat u_xp = 120.0f;
GLfloat u_yn = -60.0f;
GLfloat u_yp = 60.0f;

struct VAO {
	GLuint VertexArrayID;
	GLuint VertexBuffer;
	GLuint ColorBuffer;
	GLuint TextureBuffer;
	GLuint TextureID;

	GLenum PrimitiveMode; // GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES, GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY and GL_TRIANGLES_ADJACENCY
	GLenum FillMode; // GL_FILL, GL_LINE
	int NumVertices;
};

typedef struct VAO VAO;

struct GLMatrices {
	glm::mat4 projection;
	glm::mat4 model;
	glm::mat4 view;
	GLuint MatrixID; // For use with normal shader
	GLuint TexMatrixID; // For use with texture shader
} Matrices;

struct FTGLFont {
	FTFont* font;
	GLuint fontMatrixID;
	GLuint fontColorID;
} GL3Font;

GLuint programID, fontProgramID, textureProgramID;

/* Function to load Shaders - Use it as it is */
GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path) {

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if(VertexShaderStream.is_open())
	{
		std::string Line = "";
		while(getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if(FragmentShaderStream.is_open()){
		std::string Line = "";
		while(getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	cout << "Compiling shader : " <<  vertex_file_path << endl;
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> VertexShaderErrorMessage( max(InfoLogLength, int(1)) );
	glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
	cout << VertexShaderErrorMessage.data() << endl;

	// Compile Fragment Shader
	cout << "Compiling shader : " << fragment_file_path << endl;
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> FragmentShaderErrorMessage( max(InfoLogLength, int(1)) );
	glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
	cout << FragmentShaderErrorMessage.data() << endl;

	// Link the program
	cout << "Linking program" << endl;
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	std::vector<char> ProgramErrorMessage( max(InfoLogLength, int(1)) );
	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
	cout << ProgramErrorMessage.data() << endl;

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}

static void error_callback(int error, const char* description)
{
	cout << "Error: " << description << endl;
}

void quit(GLFWwindow *window)
{
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}

glm::vec3 getRGBfromHue (int hue)
{
	float intp;
	float fracp = modff(hue/60.0, &intp);
	float x = 1.0 - abs((float)((int)intp%2)+fracp-1.0);

	if (hue < 60)
		return glm::vec3(1,x,0);
	else if (hue < 120)
		return glm::vec3(x,1,0);
	else if (hue < 180)
		return glm::vec3(0,1,x);
	else if (hue < 240)
		return glm::vec3(0,x,1);
	else if (hue < 300)
		return glm::vec3(x,0,1);
	else
		return glm::vec3(1,0,x);
}

// /* Generate VAO, VBOs and return VAO handle */
struct VAO* create3DObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat* color_buffer_data, GLenum fill_mode=GL_FILL)
{
	struct VAO* vao = new struct VAO;
	vao->PrimitiveMode = primitive_mode;
	vao->NumVertices = numVertices;
	vao->FillMode = fill_mode;

	// Create Vertex Array Object
	// Should be done after CreateWindow and before any other GL calls
	glGenVertexArrays(1, &(vao->VertexArrayID)); // VAO
	glGenBuffers (1, &(vao->VertexBuffer)); // VBO - vertices
	glGenBuffers (1, &(vao->ColorBuffer));  // VBO - colors

	glBindVertexArray (vao->VertexArrayID); // Bind the VAO
	glBindBuffer (GL_ARRAY_BUFFER, vao->VertexBuffer); // Bind the VBO vertices
	glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW); // Copy the vertices into VBO
	glVertexAttribPointer(
						  0,                  // attribute 0. Vertices
						  3,                  // size (x,y,z)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	glBindBuffer (GL_ARRAY_BUFFER, vao->ColorBuffer); // Bind the VBO colors
	glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), color_buffer_data, GL_STATIC_DRAW);  // Copy the vertex colors
	glVertexAttribPointer(
						  1,                  // attribute 1. Color
						  3,                  // size (r,g,b)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	return vao;
}

 // Generate VAO, VBOs and return VAO handle - Common Color for all vertices 
struct VAO* create3DObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat red, const GLfloat green, const GLfloat blue, GLenum fill_mode=GL_FILL)
{
	GLfloat* color_buffer_data = new GLfloat [3*numVertices];
	for (int i=0; i<numVertices; i++) {
		color_buffer_data [3*i] = red;
		color_buffer_data [3*i + 1] = green;
		color_buffer_data [3*i + 2] = blue;
	}

	return create3DObject(primitive_mode, numVertices, vertex_buffer_data, color_buffer_data, fill_mode);
}

struct VAO* create3DTexturedObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat* texture_buffer_data, GLuint textureID, GLenum fill_mode=GL_FILL)
{
	struct VAO* vao = new struct VAO;
	vao->PrimitiveMode = primitive_mode;
	vao->NumVertices = numVertices;
	vao->FillMode = fill_mode;
	vao->TextureID = textureID;

	// Create Vertex Array Object
	// Should be done after CreateWindow and before any other GL calls
	glGenVertexArrays(1, &(vao->VertexArrayID)); // VAO
	glGenBuffers (1, &(vao->VertexBuffer)); // VBO - vertices
	glGenBuffers (1, &(vao->TextureBuffer));  // VBO - textures

	glBindVertexArray (vao->VertexArrayID); // Bind the VAO
	glBindBuffer (GL_ARRAY_BUFFER, vao->VertexBuffer); // Bind the VBO vertices
	glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW); // Copy the vertices into VBO
	glVertexAttribPointer(
						  0,                  // attribute 0. Vertices
						  3,                  // size (x,y,z)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	glBindBuffer (GL_ARRAY_BUFFER, vao->TextureBuffer); // Bind the VBO textures
	glBufferData (GL_ARRAY_BUFFER, 2*numVertices*sizeof(GLfloat), texture_buffer_data, GL_STATIC_DRAW);  // Copy the vertex colors
	glVertexAttribPointer(
						  2,                  // attribute 2. Textures
						  2,                  // size (s,t)
						  GL_FLOAT,           // type
						  GL_FALSE,           // normalized?
						  0,                  // stride
						  (void*)0            // array buffer offset
						  );

	return vao;
}

/* Render the VBOs handled by VAO */
void draw3DObject (struct VAO* vao)
{
	// Change the Fill Mode for this object
	glPolygonMode (GL_FRONT_AND_BACK, vao->FillMode);

	// Bind the VAO to use
	glBindVertexArray (vao->VertexArrayID);

	// Enable Vertex Attribute 0 - 3d Vertices
	glEnableVertexAttribArray(0);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->VertexBuffer);

	// Enable Vertex Attribute 1 - Color
	glEnableVertexAttribArray(1);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->ColorBuffer);

	// Draw the geometry !
	glDrawArrays(vao->PrimitiveMode, 0, vao->NumVertices); // Starting from vertex 0; 3 vertices total -> 1 triangle
}

void draw3DTexturedObject (struct VAO* vao)
{
	// Change the Fill Mode for this object
	glPolygonMode (GL_FRONT_AND_BACK, vao->FillMode);

	// Bind the VAO to use
	glBindVertexArray (vao->VertexArrayID);

	// Enable Vertex Attribute 0 - 3d Vertices
	glEnableVertexAttribArray(0);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->VertexBuffer);

	// Bind Textures using texture units
	glBindTexture(GL_TEXTURE_2D, vao->TextureID);

	// Enable Vertex Attribute 2 - Texture
	glEnableVertexAttribArray(2);
	// Bind the VBO to use
	glBindBuffer(GL_ARRAY_BUFFER, vao->TextureBuffer);

	// Draw the geometry !
	glDrawArrays(vao->PrimitiveMode, 0, vao->NumVertices); // Starting from vertex 0; 3 vertices total -> 1 triangle

	// Unbind Textures to be safe
	glBindTexture(GL_TEXTURE_2D, 0);
}
void updateProjection()
  {
      Matrices.projection = glm::ortho(u_xn, u_xp, u_yn, u_yp, 0.1f, 500.0f);
  }

  void zoomin()
  {
    u_xn += 3.0f;
    u_xp -= 3.0f;
    u_yn += 1.5f;
    u_yp -= 1.5f;
  }

  void zoomout()
  {
    u_xn -= 3.0f;
    u_xp += 3.0f;
    u_yn -= 1.5f;
    u_yp += 1.5f;
  }

/* Create an OpenGL Texture from an image */
GLuint createTexture (const char* filename)
{
	GLuint TextureID;
	// Generate Texture Buffer
	glGenTextures(1, &TextureID);
	// All upcoming GL_TEXTURE_2D operations now have effect on our texture buffer
	glBindTexture(GL_TEXTURE_2D, TextureID);
	// Set our texture parameters
	// Set texture wrapping to GL_REPEAT
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// Set texture filtering (interpolation)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// Load image and create OpenGL texture
	int twidth, theight;
	unsigned char* image = SOIL_load_image(filename, &twidth, &theight, 0, SOIL_LOAD_RGB);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	glGenerateMipmap(GL_TEXTURE_2D); // Generate MipMaps to use
	SOIL_free_image_data(image); // Free the data read from file after creating opengl texture
	glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess it up

	return TextureID;
}


/**************************
 * Customizable functions *
 **************************/

/* Executed when a regular key is pressed/released/held-down */
/* Prefered for Keyboard events */
void keyboard (GLFWwindow* window, int key, int scancode, int action, int mods)
{
	// Function is called first on GLFW_PRESS.

	if (action == GLFW_RELEASE) {
		switch (key) {
			case GLFW_KEY_S:
				speed /= 1.5; 
				break;

			case GLFW_KEY_D:
				speed/= 0.67;
				break;

			case GLFW_KEY_SPACE:
				if (!jump)
				{
				jump = 1;
				initial_time = glfwGetTime();				
				}
				break;
		
			case GLFW_KEY_T:
				camera = 1;
				break;

			case GLFW_KEY_N:
				hx--;
				break;

			case GLFW_KEY_M:
				hx++;
				break;

			case GLFW_KEY_I:
				hy++;
				break;

			case GLFW_KEY_K:
				hy--;
				break;			

			case GLFW_KEY_Y:
				camera = 2;
				break;

			case GLFW_KEY_F:
				camera = 3;
				break;

			case GLFW_KEY_G:
				camera = 4;
				break;

			case GLFW_KEY_H:
				camera = 5;
				break;	

			default:
				break;
		}
	}
	else if (action == GLFW_PRESS) {
		switch (key) {
			case GLFW_KEY_ESCAPE:
				quit(window);
				break;
			default:
				break;
		}
	}
	else if(action == GLFW_REPEAT)
      {
        switch(key)
        {
          case GLFW_KEY_P:
            zoomin();
            updateProjection();
            break;

          case GLFW_KEY_L:
            zoomout();
            updateProjection();
            break;

           case GLFW_KEY_UP:
				float t;
				Y+=speed;
				if (undo_keyboard)
				{
					Y-=speed;
					undo_keyboard = 0;
				}			
				break;

          	case GLFW_KEY_DOWN:
				Y-=speed;
				if (undo_keyboard)
				{
					Y+=speed;
					undo_keyboard = 0;
				}
				break;

			case GLFW_KEY_RIGHT:
				X+=speed;
				if (undo_keyboard)
				{
					X-=speed;
					undo_keyboard = 0;
				}
				break;

		  	case GLFW_KEY_LEFT:
				X-=speed;
				if (undo_keyboard)
				{
					X+=speed;
					undo_keyboard = 0;
				}
				break;

          // case GLFW_KEY_LEFT:
          // move_left();
          // updateProjection();
          // break;
          
          // case GLFW_KEY_RIGHT:
          // move_right();
          // updateProjection();
          // break;
        }
      
	}
}

/* Executed for character input (like in text boxes) */
void keyboardChar (GLFWwindow* window, unsigned int key)
{
	switch (key) {
		case 'Q':
		case 'q':
			quit(window);
			break;
		default:
			break;
	}
}

/* Executed when a mouse button is pressed/released */
void mouseButton (GLFWwindow* window, int button, int action, int mods)
{
	switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT:
			
    		glfwGetCursorPos(window, &xpos, &ypos);
    		// glfwSetCursorPosCallback(window, &xpos, &ypos);
    		// cout<<xpos<<"and"<<ypos<<endl;
			break;


		case GLFW_MOUSE_BUTTON_RIGHT:
			// if (action == GLFW_RELEASE) {
				// cube_rot_dir *= -1;
			// }
			break;
		default:
			break;
	}
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
  if(yoffset > 0)
    zoomin();
  else
    zoomout();
  updateProjection();
}
void cursor_pos_callback(GLFWwindow* window, double x_pos, double y_pos)
{	
	glfwGetCursorPos(window, &xpos, &ypos);
	cout<<xpos<<"and"<<ypos<<endl;
}



/* Executed when window is resized to 'width' and 'height' */
/* Modify the bounds of the screen here in glm::ortho or Field of View in glm::Perspective */
void reshapeWindow (GLFWwindow* window, int width, int height)
{
	int fbwidth=width, fbheight=height;
	/* With Retina display on Mac OS X, GLFW's FramebufferSize
	 is different from WindowSize */
	glfwGetFramebufferSize(window, &fbwidth, &fbheight);

	GLfloat fov = 90.0f;

	
	glfwGetCursorPos(window, &xpos, &ypos);	// sets the viewport of openGL renderer
	glViewport (0, 0, (GLsizei) fbwidth, (GLsizei) fbheight);

	// set the projection matrix as perspective
	 // glMatrixMode (GL_PROJECTION);
	 // glLoadIdentity ();
	 // gluPerspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1, 500.0); 
	// Store the projection matrix in a variable for future use
	// Perspective projection for 3D views
	// Matrices.projection = glm::perspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1f, 500.0f);

	// Ortho projection for 2D views
	Matrices.projection = glm::ortho(-4.0f, 4.0f, -4.0f, 4.0f, 0.1f, 500.0f);
}

// VAO *triangle, *rectangle;
// Creates the triangle object used in this sample code

class Cube
  {
    private:
      float m_x, m_y, m_z, m_length, m_width, m_height;
      // float m_minanglem, m_maxangle;
      VAO *m_cube;
      GLfloat m_vertex_buffer_data[3 * cubeVertex];
      // GLfloat  m_color_buffer_data[3 * cubeVertex];
      float m_texture_buffer_data [2* cubeVertex];
    
    public:
      // Cube(float x, float y, float z, float length, float width, float height);
      void getValues(float x, float y, float z, float length, float width, float height);
      void createCube(const char *);
      // void changeAngle(float angle);
      void draw();

  };

  void Cube::getValues(float x, float y, float z,float length, float width, float height)
  {
    m_x = x;
    m_y = y;
    m_z = z;

    m_length = length;
    m_width = width;
    m_height = height;

  }

  void Cube::createCube(const char *texture )
  {
	m_vertex_buffer_data [0] = 0;
	m_vertex_buffer_data [1] = 0;
	m_vertex_buffer_data [2] = 0;

	m_vertex_buffer_data [3] = m_length;
	m_vertex_buffer_data [4] = 0;
	m_vertex_buffer_data [5] = 0;

	m_vertex_buffer_data [6] = m_length;
	m_vertex_buffer_data [7] = 0;
	m_vertex_buffer_data [8] = m_height;

	m_vertex_buffer_data [9] = m_length;
	m_vertex_buffer_data [10] = 0;
	m_vertex_buffer_data [11] = m_height;

	m_vertex_buffer_data [12] = 0;
	m_vertex_buffer_data [13] = 0;
	m_vertex_buffer_data [14] = m_height;

	m_vertex_buffer_data [15] = 0;
	m_vertex_buffer_data [16] = 0;
	m_vertex_buffer_data [17] = 0;
// bottom rectangle down!


	m_vertex_buffer_data [18] = 0; 
	m_vertex_buffer_data [19] = m_width;
	m_vertex_buffer_data [20] = 0;

	m_vertex_buffer_data [21] = m_length;
	m_vertex_buffer_data [22] = m_width;
	m_vertex_buffer_data [23] = 0;

	m_vertex_buffer_data [24] = m_length;
	m_vertex_buffer_data [25] = m_width;
	m_vertex_buffer_data [26] = m_height;

	m_vertex_buffer_data [27] = m_length;
	m_vertex_buffer_data [28] = m_width;
	m_vertex_buffer_data [29] = m_height;

	m_vertex_buffer_data [30] = 0;
	m_vertex_buffer_data [31] = m_width;
	m_vertex_buffer_data [32] = m_height;

	m_vertex_buffer_data [33] = 0;
	m_vertex_buffer_data [34] = m_width;
	m_vertex_buffer_data [35] = 0;
// Top rectangle done!

	m_vertex_buffer_data [36] = 0;
	m_vertex_buffer_data [37] = 0;
	m_vertex_buffer_data [38] = 0;

	m_vertex_buffer_data [39] = 0;
	m_vertex_buffer_data [40] = m_width;
	m_vertex_buffer_data [41] = 0;

	m_vertex_buffer_data [42] = 0;
	m_vertex_buffer_data [43] = m_width;
	m_vertex_buffer_data [44] = m_height;

	m_vertex_buffer_data [45] = 0;
	m_vertex_buffer_data [46] = m_width;
	m_vertex_buffer_data [47] = m_height;

	m_vertex_buffer_data [48] = 0;
	m_vertex_buffer_data [49] = 0;
	m_vertex_buffer_data [50] = m_height;

	m_vertex_buffer_data [51] = 0;
	m_vertex_buffer_data [52] = 0;
	m_vertex_buffer_data [53] = 0;
//left done


	m_vertex_buffer_data [54] = m_length;
	m_vertex_buffer_data [55] = 0;
	m_vertex_buffer_data [56] = 0;

	m_vertex_buffer_data [57] = m_length;
	m_vertex_buffer_data [58] = m_width;
	m_vertex_buffer_data [59] = 0;

	m_vertex_buffer_data [60] = m_length;
	m_vertex_buffer_data [61] = m_width;
	m_vertex_buffer_data [62] = m_height;

	m_vertex_buffer_data [63] = m_length;
	m_vertex_buffer_data [64] = m_width;
	m_vertex_buffer_data [65] = m_height;

	m_vertex_buffer_data [66] = m_length;
	m_vertex_buffer_data [67] = 0;
	m_vertex_buffer_data [68] = m_height;

	m_vertex_buffer_data [69] = m_length;
	m_vertex_buffer_data [70] = 0;
	m_vertex_buffer_data [71] = 0;
	//right done

	m_vertex_buffer_data [72] = 0;
	m_vertex_buffer_data [73] = m_width;
	m_vertex_buffer_data [74] = 0;

	m_vertex_buffer_data [75] = m_length;
	m_vertex_buffer_data [76] = m_width;
	m_vertex_buffer_data [77] = 0;

	m_vertex_buffer_data [78] = m_length;
	m_vertex_buffer_data [79] = 0;
	m_vertex_buffer_data [80] = 0;

	m_vertex_buffer_data [81] = m_length;
	m_vertex_buffer_data [82] = 0;
	m_vertex_buffer_data [83] = 0;

	m_vertex_buffer_data [84] = 0;
	m_vertex_buffer_data [85] = 0;
	m_vertex_buffer_data [86] = 0;

	m_vertex_buffer_data [87] = 0;
	m_vertex_buffer_data [88] = m_width;
	m_vertex_buffer_data [89] = 0;
//back done

	m_vertex_buffer_data [90] = 0;
	m_vertex_buffer_data [91] = m_width;
	m_vertex_buffer_data [92] = m_height;

	m_vertex_buffer_data [93] = m_length;
	m_vertex_buffer_data [94] = m_width;
	m_vertex_buffer_data [95] = m_height;

	m_vertex_buffer_data [96] = m_length;
	m_vertex_buffer_data [97] = 0;
	m_vertex_buffer_data [98] = m_height;

	m_vertex_buffer_data [99] = m_length;
	m_vertex_buffer_data [100] = 0;
	m_vertex_buffer_data [101] = m_height;

	m_vertex_buffer_data [102] = 0;
	m_vertex_buffer_data [103] = 0;
	m_vertex_buffer_data [104] = m_height;

	m_vertex_buffer_data [105] = 0;
	m_vertex_buffer_data [106] = m_width;
	m_vertex_buffer_data [107] = m_height;
	//front done

	

    glActiveTexture(GL_TEXTURE0);
	// load an image file directly as a new OpenGL texture
	// GLuint texID = SOIL_load_OGL_texture ("brick.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_TEXTURE_REPEATS); // Buggy for OpenGL3
	textureID = createTexture(texture);
	// check for an error during the load process
	if(textureID == 0)
		cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

		m_texture_buffer_data [0] = 0;
		m_texture_buffer_data [1] = 0;
		m_texture_buffer_data [2] = 1;
		m_texture_buffer_data [3] = 0;
		m_texture_buffer_data [4] = 1;
		m_texture_buffer_data [5] = 1;

		m_texture_buffer_data [6] = 1;
		m_texture_buffer_data [7] = 1;
		m_texture_buffer_data [8] = 0;
		m_texture_buffer_data [9] = 1;
		m_texture_buffer_data [10] = 0;
		m_texture_buffer_data [11] = 0;

		m_texture_buffer_data [12] = 0;
		m_texture_buffer_data [13] = 0;
		m_texture_buffer_data [14] = 1;
		m_texture_buffer_data [15] = 0;
		m_texture_buffer_data [16] = 1;
		m_texture_buffer_data [17] = 1;

		m_texture_buffer_data [18] = 1;
		m_texture_buffer_data [19] = 1;
		m_texture_buffer_data [20] = 0;
		m_texture_buffer_data [21] = 1;
		m_texture_buffer_data [22] = 0;
		m_texture_buffer_data [23] = 0;

		m_texture_buffer_data [24] = 0;
		m_texture_buffer_data [25] = 0;
		m_texture_buffer_data [26] = 1;
		m_texture_buffer_data [27] = 0;
		m_texture_buffer_data [28] = 1;
		m_texture_buffer_data [29] = 1;

		m_texture_buffer_data [30] = 1;
		m_texture_buffer_data [31] = 1;
		m_texture_buffer_data [32] = 0;
		m_texture_buffer_data [33] = 1;
		m_texture_buffer_data [34] = 0;
		m_texture_buffer_data [35] = 0;

		m_texture_buffer_data [36] = 0;
		m_texture_buffer_data [37] = 0;
		m_texture_buffer_data [38] = 1;
		m_texture_buffer_data [39] = 0;
		m_texture_buffer_data [40] = 1;
		m_texture_buffer_data [41] = 1;

		m_texture_buffer_data [42] = 1;
		m_texture_buffer_data [43] = 1;
		m_texture_buffer_data [44] = 0;
		m_texture_buffer_data [45] = 1;
		m_texture_buffer_data [46] = 0;
		m_texture_buffer_data [47] = 0;

		m_texture_buffer_data [48] = 0;
		m_texture_buffer_data [49] = 0;
		m_texture_buffer_data [50] = 1;
		m_texture_buffer_data [51] = 0;
		m_texture_buffer_data [52] = 1;
		m_texture_buffer_data [53] = 1;

		m_texture_buffer_data [54] = 1;
		m_texture_buffer_data [55] = 1;
		m_texture_buffer_data [56] = 0;
		m_texture_buffer_data [57] = 1;
		m_texture_buffer_data [58] = 0;
		m_texture_buffer_data [59] = 0;

		m_texture_buffer_data [60] = 0;
		m_texture_buffer_data [61] = 0;
		m_texture_buffer_data [62] = 1;
		m_texture_buffer_data [63] = 0;
		m_texture_buffer_data [64] = 1;
		m_texture_buffer_data [65] = 1;

		m_texture_buffer_data [66] = 1;
		m_texture_buffer_data [67] = 1;
		m_texture_buffer_data [68] = 0;
		m_texture_buffer_data [69] = 1;
		m_texture_buffer_data [70] = 0;
		m_texture_buffer_data [71] = 0;

    
    // m_cube = create3DObject(GL_TRIANGLES, cubeVertex, m_vertex_buffer_data, m_color_buffer_data, GL_FILL);
	// create3DTexturedObject creates and returns a handle to a VAO that can be used later
	m_cube = create3DTexturedObject(GL_TRIANGLES, cubeVertex, m_vertex_buffer_data, m_texture_buffer_data, textureID, GL_FILL);
  }

 
// float camera_rotation_angle = 0;
float cube_rotation = 0;
// float triangle_rotation = 0;

// you have to manage this fucntion to make it work
  void Cube::draw()
  {
  	// std::cout<<endl<<"cube draw";
    glm::mat4 VP = Matrices.projection * Matrices.view;

      glm::mat4 MVP;
      Matrices.model = glm::mat4(1.0f);
      
      // std::cout << rectangle_rotation <<endl;
      glm::mat4 translateCube = glm::translate (glm::vec3(m_x, m_y , m_z));        // glTranslatef
      
      glm::mat4 rotateCube = glm::rotate((float)(cube_rotation*M_PI/180.0f), glm::vec3(0,0,1)); // rotate about vector (-1,1,1)
      Matrices.model *= (translateCube * rotateCube);
      MVP = VP * Matrices.model;
      glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);

	 draw3DTexturedObject(m_cube);
  // Increment angles
      // float increments = 1;
      // camera_rotation_angle+= 0.001; // Simulating camera rotation
  }


 void drawBasics()
  {
  	// std::cout<<"Basics";
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    // glUseProgram (programID);
    glUseProgram(textureProgramID);

    glm::vec3 eye;
	glm::vec3 target;
	glm::vec3 up( 0, 1, 0);
    
    // Eye - Location of camera. Don't change unless you are sure!!
	if (camera == 1)
	{
    eye = glm::vec3(0 , 0 ,100);
    target = glm::vec3(0,0,0);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    up = glm::vec3(0, 1, 0);
	}

	else if (camera == 2){
    eye = glm::vec3(-7 , -7 , 20);
    target = glm::vec3(0,0,-5);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    up = glm::vec3(0, 1, 0);
	}

	else if (camera == 3){
    eye = glm::vec3(X, Y ,Z + 5);
    target = glm::vec3(X+5,Y+5,-10);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    up = glm::vec3(0, 1, 0);
	}

	else if (camera == 4){

    eye = glm::vec3(X+1.5, Y+1.5, Z+3);
    target = glm::vec3(5,5,Z+2);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    up = glm::vec3(0, 1, 0);
	}

	// else if (camera == 5)
	// {
 //    eye = glm::vec3(hx, hy, 20);
 //    cout<<"camera"<<hx<<"and"<<hy<<endl;
 //    target = glm::vec3(0,0,0);
 //    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
 //    up = glm::vec3(0, 1, 0);
	// }

	else if (camera == 5)
	{
		float xplot,yplot;
		float m,y,x;
		//first translate the y as board cordinate;
		// ypos*(-1)

		y = ( ((ypos - 300) * 16) /300 ) *(-1);
		x = (xpos - 400) / 50 ;

		m = (y/x);

    eye = glm::vec3(x, y, 20);
    cout<<"translated"<<x<<"and"<<y<<endl;
    cout<<"pointer"<<xpos<<"and"<<ypos<<endl;
    target = glm::vec3(0,0,0);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    up = glm::vec3(0, 1, 0);
	}


	else
		camera = 1;


    // Compute Camera matrix (view)
    Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
    //  Don't change unless you are sure!!
    // Matrices.view = glm::lookAt(glm::vec3(0,0,3), glm::vec3(0,0,0), glm::vec3(0,1,0)); // Fixed camera for 2D (ortho) in XY plane
    // std::cout<<"basics complete";
  }


/* Initialise glfw window, I/O callbacks and the renderer to use */
/* Nothing to Edit here */
GLFWwindow* initGLFW (int width, int height)
{
	GLFWwindow* window; // window desciptor/handle

	glfwSetErrorCallback(error_callback);
	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(width, height, "Sample OpenGL 3.3 Application", NULL, NULL);

	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
	glfwSwapInterval( 1 );

	/* --- register callbacks with GLFW --- */

	/* Register function to handle window resizes */
	/* With Retina display on Mac OS X GLFW's FramebufferSize
	 is different from WindowSize */
	glfwSetFramebufferSizeCallback(window, reshapeWindow);
	glfwSetWindowSizeCallback(window, reshapeWindow);

	/* Register function to handle window close */
	glfwSetWindowCloseCallback(window, quit);

	/* Register function to handle keyboard input */
	glfwSetKeyCallback(window, keyboard);      // general keyboard input
	glfwSetCharCallback(window, keyboardChar);  // simpler specific character handling

	/* Register function to handle mouse click */
	glfwSetMouseButtonCallback(window, mouseButton);  // mouse button clicks
	glfwSetCursorPosCallback(window, cursor_pos_callback);

	glfwSetScrollCallback(window, scroll_callback);

	return window;
}

/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL (GLFWwindow* window, int width, int height)
{
	// Load Textures
	// Enable Texture0 as current texture memory
	// glActiveTexture(GL_TEXTURE0);
	// load an image file directly as a new OpenGL texture
	// GLuint texID = SOIL_load_OGL_texture ("brick.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_TEXTURE_REPEATS); // Buggy for OpenGL3
	// textureID = createTexture("brick.png");
	// check for an error during the load process
	// if(textureID == 0)
		// cout << "SOIL loading error: '" << SOIL_last_result() << "'" << endl;

	// Create and compile our GLSL program from the texture shaders
	textureProgramID = LoadShaders( "TextureRender.vert", "TextureRender.frag" );
	// Get a handle for our "MVP" uniform
	Matrices.TexMatrixID = glGetUniformLocation(textureProgramID, "MVP");


	/* Objects should be created before any other gl function and shaders */
	// Create the models
	// createTriangle (); // Generate the VAO, VBOs, vertices data & copy into the array buffer
	// createRectangle (textureID);


	// Create and compile our GLSL program from the shaders
	programID = LoadShaders( "Sample_GL3.vert", "Sample_GL3.frag" );
	// Get a handle for our "MVP" uniform
	Matrices.MatrixID = glGetUniformLocation(programID, "MVP");


	reshapeWindow (window, width, height);

	// Background color of the scene
	glClearColor (0.0f, 0.4f, 1.0f, 0.0f); // R, G, B, A
	glClearDepth (1.0f);

	glEnable (GL_DEPTH_TEST);
	glDepthFunc (GL_LESS);
	// glEnable(GL_BLEND);
	// glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Initialise FTGL stuff
	const char* fontfile = "arial.ttf";
	GL3Font.font = new FTExtrudeFont(fontfile); // 3D extrude style rendering

	if(GL3Font.font->Error())
	{
		cout << "Error: Could not load font `" << fontfile << "'" << endl;
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// Create and compile our GLSL program from the font shaders
	fontProgramID = LoadShaders( "fontrender.vert", "	fontrender.frag" );
	GLint fontVertexCoordAttrib, fontVertexNormalAttrib, fontVertexOffsetUniform;
	fontVertexCoordAttrib = glGetAttribLocation(fontProgramID, "vertexPosition");
	fontVertexNormalAttrib = glGetAttribLocation(fontProgramID, "vertexNormal");
	fontVertexOffsetUniform = glGetUniformLocation(fontProgramID, "pen");
	GL3Font.fontMatrixID = glGetUniformLocation(fontProgramID, "MVP");
	GL3Font.fontColorID = glGetUniformLocation(fontProgramID, "fontColor");

	GL3Font.font->ShaderLocations(fontVertexCoordAttrib, fontVertexNormalAttrib, fontVertexOffsetUniform);
	GL3Font.font->FaceSize(1);
	GL3Font.font->Depth(0);
	GL3Font.font->Outset(0, 0);
	GL3Font.font->CharMap(ft_encoding_unicode);

	cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
	cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
	cout << "VERSION: " << glGetString(GL_VERSION) << endl;
	cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

}
void fall(){
	Z-=0.1;
}

void respawn()
{
	X = -5;
	Y = -5;
	Z = 1;
}
void drop()
{
	Z-=0.06;
}

void sound_explode()
{
	if(!buffer.openFromFile("explode.wav"))
          cout<<"Failed";
        buffer.play();
}

void sound_water()
{
	if(!buffer.openFromFile("water.wav"))
          cout<<"Failed";
        buffer.play();
}

void sound_coin()
{
	if(!buffer.openFromFile("coin.wav"))
          cout<<"Failed";
        buffer.play();
}

void sound_victory()
{
	if(!buffer.openFromFile("victory.wav"))
          cout<<"Failed";
        buffer.play();
}

int main (int argc, char** argv)
{
	time_t t;
	int width = 800;
	int height = 600;

	GLFWwindow* window = initGLFW(width, height);

	initGL (window, width, height);

	// double last_update_time = glfwGetTime(), current_time;
	
	vector <Cube *> field;
	vector <Cube *> water;
	vector <Cube *> bomb;

	Cube coin1;
	Cube coin2;
	Cube coin3;

	vector <Cube *> princess;
	vector <Cube *> tiles;

	char box[] = "gold.jpg";
	char p[] = "player.png";
	char b[] = "brick.png";
	char w[] = "water.png";
	char bo[] = "bomb.png";
	char ex[] = "explode.png";
	char pr[] = "princess.png";
	char fire[] = "fire.jpg";
	// float iter = 0;
	for(int i=0; i<20; i++)
	{
		for(int j=0;j<20; j++)
		{
			// cout<<iter++<<endl;
			if (i >= 5 && i <15 && j>= 5 && j<15){

				if ((j == 12 && i == 12) || (j==10 && i==8) || (j==12 && i ==10) || (j==8 && i==10)||
					 (j==10 && i ==12) || (i==8 && j==8) || (j==13 && i== 7)
					|| (j==13 && i==12) || (j==7 && i==7)  || (j==6 && i==10) || (j == 13 && i ==13 )
					|| (j == 11 && i == 6) || (j==6  && i == 14))
					continue;

				else if ( (j == 7 && i ==13 ) || (j == 10 && i ==10 )
				 	|| (j == 11 && i ==12 ) || (j == 8 && i ==11 ) || (j == 12 && i == 8 )
					)
				{
					Cube *temp = new Cube;
				//first cube at -5,-10,0
					temp->getValues(-10+i,-10+j,1,1,1,1);
					temp ->createCube(bo);
					bomb.push_back(temp);
					continue;
				}
				else if (j == 14 && i ==14)
				{
					Cube *temp = new Cube;
					//first cube at -5,-10,0
					temp->getValues(-10+i,-10+j,1,1,1,1);
					temp->createCube(pr);
					princess.push_back(temp);
					continue;					
				}
				
				else{

					Cube *temp = new Cube;
					//first cube at -5,-10,0
					temp->getValues(-10+i,-10+j,0,1,1,1);
					temp->createCube(b);
					field.push_back(temp);
					continue;
				}
			}

			else{
				Cube *temp = new Cube;
				//first cube at -5,-10,0

				temp->getValues(-10+i,-10+j,-1,1,1,1);
				temp ->createCube(w);
				water.push_back(temp);
			}
		}

	}

	Cube move_tile1;
	Cube move_tile2;

	Cube fireball;
	Cube fireball1;
	Cube fireball2;
	Cube fireball3;
	Cube fireball4;
	Cube fireball5;
	Cube explode;
	Cube player;

	move_tile1.getValues(-4,1,motion,1,1,1);
	move_tile2.getValues(4,-4,motion,1,1,1);


	fireball.getValues(2,3,motion,1,1,1);
	fireball1.getValues(-2,0,motion,1,1,1);
	fireball2.getValues(2,0,motion,1,1,1);
	fireball3.getValues(3,3,motion,1,1,1);
	fireball4.getValues(0,-2,motion,1,1,1);
	fireball5.getValues(0,-4,motion,1,1,1);
	
	coin1.getValues(gold_x[0],gold_y[0],1,1,1,1);
	coin2.getValues(gold_x[1],gold_y[1],1,1,1,1);
	coin3.getValues(gold_x[2],gold_y[2],1,1,1,1);
	
	coin1.createCube(box);
	coin2.createCube(box);
	coin3.createCube(box);

	explode.getValues(0,0,1,1,1,1);
	player.getValues(-5,-5,1,1,1,1);


	move_tile1.createCube(b);
	move_tile2.createCube(b);

	fireball.createCube(fire);
	fireball1.createCube(fire);
	fireball2.createCube(fire);
	fireball3.createCube(fire);
	fireball4.createCube(fire);
	fireball5.createCube(fire);
	explode.createCube(ex);
	player.createCube(p);


	float bkp_time =glfwGetTime();
	bkp_Z = Z;
	/* Draw in loop */
	int sound = 0;
	long long int count = 0;
	while (!glfwWindowShouldClose(window)) {
		
		// cout<<X<<"and"<<Y<<endl;
		srand((unsigned) time(&t));

		float rand_i1 = (rand()%7) + 3;
		float rand_j1 = (rand()%7) + 3;

		// // std::cout <<"loop"<<endl;
		// // PlaySound("game.wav", NULL, SND_ASYNC|SND_FILENAME|SND_LOOP);
		//  // PlaySound("game.wav", NULL, SND_FILENAME|SND_LOOP);

		move_tile1.getValues(-4,1,tile_motion,1,1,1);
		move_tile2.getValues(4,-4,tile_motion,1,1,1);

		fireball.getValues(2,3,motion,1,1,1);
		fireball1.getValues(-2,0,motion,1,1,1);
		fireball2.getValues(2,0,motion,1,1,1);
		fireball3.getValues(-3,3,motion,1,1,1);
		fireball4.getValues(0,-2,motion,1,1,1);
		fireball5.getValues(0,2,motion,1,1,1);

		player.getValues(X,Y,Z,1,1,1);

		explode.getValues(-5+rand_i1,-5+rand_j1,1,1,1,1);				

		if (motion <= -2)
			sign = 1;

		if (motion >= 3)
			sign = -1;

		motion = motion + ( sign *  0.05);

		if (tile_motion <= 0)
			tile_sign = 1;

		if (tile_motion >= 5)
			tile_sign = -1;

		tile_motion = tile_motion + ( tile_sign *  0.05);
				
		// cout <<motion <<endl;
		
		drawBasics();	

		float u = 10;

		if (jump)
		{
			float t;
			t = glfwGetTime() - initial_time;

			Z = (u*t - 5*t*t) +  1;

			if (Z <= 1.001)
			{	
				Z  = 1;	
				jump = 0;
			}
			
		}


		move_tile1.draw();
		move_tile2.draw();


    	fireball.draw();
    	fireball1.draw();
    	fireball2.draw();
    	fireball3.draw();
    	fireball4.draw();
    	fireball5.draw();

		

		for(vector<Cube *>::iterator it = water.begin(); it != water.end(); ++it)
        	{(*it)->draw();
        		
        		}

		for(vector<Cube *>::iterator it = field.begin(); it != field.end(); ++it)
        	{(*it)->draw();
        		
        	}

		for(vector<Cube *>::iterator it = bomb.begin(); it != bomb.end(); ++it)
        	{
        		(*it)->draw();
        		
        	}
   		
   		
        for(vector<Cube *>::iterator it = tiles.begin(); it != tiles.end(); ++it)
        	{
        		(*it)->draw();
   				
   			}


        for(vector<Cube *>::iterator it = princess.begin(); it != princess.end(); ++it)
        	{
        		(*it)->draw();
   				
   			}

   		if (c1 != 0)
   			coin1.draw();
   		if(c2 != 0)
   			coin2.draw();
   		if (c3 != 0)
   			coin3.draw();

    	player.draw();
    	explode.draw();

    	// Game...

    	if (X < -5.5 || X >4.5 || Y > 4.5 || Y < -5.5)
    		{
    			sound_water();
    			fall();
    		}
    	if(Z < -4)
    	{
    		sound_water;
    		respawn();
    	}

    	// at every jump count the h
    	
    	//check_gold

    	if ( X > (gold_x[0]-1) && X<(gold_x[0] + 1) &&
    		Y > (gold_y[0]-1) && Y<(gold_y[0] + 1 ) && Z > 0 && Z < 2)
    		{
    			c1 = 0;
    			if(!s1){
    				sound_coin();
    				s1++;
    			}
    		}

    	if ( X > (gold_x[1]-1) && X<(gold_x[1] + 1) &&
    		Y > (gold_y[1]-1) && Y<(gold_y[1] + 1 ) && Z > 0 && Z < 2)
    		{
    			c2 = 0;
    			if (!s2){
    				sound_coin();
    				s2++;
    			}
    		}

    	if ( X > (gold_x[2]-1) && X<(gold_x[2] + 1) &&
    		Y > (gold_y[2]-1) && Y<(gold_y[2] + 1 ) && Z > 0 && Z < 2)
    		{
    			c3 = 0;
    			if(!s3)
    			{	
    				s3++;
    				sound_coin();
    			}
    		}
    	//check ghost
    	for(int i=0;i<5;i++)
    	{
    		if ( X > (ghost_x[i]-1) && X<(ghost_x[i] + 1) &&
    		Y > (ghost_y[i]-1) && Y<(ghost_y[i] + 1 )  && Z > 0 && Z < 2) 
    			{
    				respawn();
    				sound_explode();
    			}
    	}

    	//check ditchessssssssss
    	for (int i=0;i<5;i++){

    		if (X > (spaces_x[i]-0.5) && X < (spaces_x[i]+0.5) && Y > (spaces_y[i] - 0.5) &&
    		 	Y < (spaces_y[i]+0.5) )
    			{
    				fall();
    			if ( Z < -1){
    				// cout<<"i was here"<<endl;
    				sound_water();
    				respawn();
    			}
    		}
    	}

    	// Handle the tiles............!!!!!!!!1
    	for (int i=0;i<2;i++){
    		if ( X > (Movtile_x[i]-1) && X<(Movtile_x[i] + 1) &&
    		Y > (Movtile_y[i]-1) && Y<(Movtile_y[i] + 1 ))
    			{
    				
    				if (tile_motion > 1 && Z > 0.99999 && Z <=1.000001 )
    					undo_keyboard = 1;
    				
    				else{
	    				Z = 1+tile_motion;
    					if (Z <= 1)
    						Z=1;
    				}
    			//to be handled
    		}
    	}


    	if( jump ==0)
    		for (int i=0;i<2;i++){
	    		if (Z > 1.001)
	    		{
	    			if ( X > (Movtile_x[i]-1) && X<(Movtile_x[i] + 1) &&
	    					Y > (Movtile_y[i]-1) && Y<(Movtile_y[i] + 1 ))
	    				cout<<"";

	    			else
	    				drop();
	    		}
    		}
 	
    	// check boom-5+rand_i1,-5+rand_j1

    	if ( X > (rand_i1-6) && X<(rand_i1 - 4) &&
    		  Y > (rand_j1-6) && Y<(rand_j1 - 4) && Z > 0 && Z < 2)
    	{
    			sound_explode();
    			respawn();
    	}

    	//next stage

    	if ( X > 3.5 && X < 4.5 &&
    		  Y > 3.5 && Y < 4.5 && Z > 0 && Z < 2)
    			{
    				sound_victory();
    				respawn();
    			}

    	for(int i=0;i<5;i++)
    	{
    		if ( X > (tiles_x[i]-1) && X<(tiles_x[i] + 1) &&
    		Y > (tiles_y[i]-1) && Y<(tiles_y[i] + 1 )  && Z > (motion-1) && Z < (motion+1)) 
    		{
    			sound_explode();
    			respawn();
    		}
    	}	

    	// Poll for Keyboard and mouse events
		glfwPollEvents();
		
    // 	if (!sound){
    // 	if(!buffer.openFromFile("fire.wav"))
    //       cout<<"Failed";
    //     buffer.play();
    //     sound ++;
    // }

   		glfwSwapBuffers(window);
		}
	
	glfwTerminate();
	exit(EXIT_SUCCESS);
}